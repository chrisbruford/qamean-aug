let EventEmitter = require('events');

class Counter extends EventEmitter {
    constructor() {
        super()

        let n = 0;
        this.counter = () => {
            setInterval(()=>this.emit('count', ++n), 1000);
        }
        this.message = () => {
            this.emit("message","You have mail");
        }

    }
}

let myCounter = new Counter();

myCounter.on('count',count=>console.log(count));
myCounter.on('message',message=>console.log(message));

myCounter.counter();
myCounter.message();

