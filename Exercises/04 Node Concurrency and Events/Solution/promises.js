let printArray = (array) => {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            if (Array.isArray(array)) {
                resolve(array.join(" "));
            } else {
                reject("Not an array");
            }
        }, 3000)
    })
}

printArray(['This','was','an','array'])
.then(result => console.log(result))
.catch(result => console.log(result));

printArray('This is not an array')
.then(result => console.log(result))
.catch(result => console.log(result));