const EventEmitter = require('events').EventEmitter;

class Vehicle extends EventEmitter {
    constructor(make, model, cost, colour) {
        super()
        this._make = make;
        this._model = model;
        this._cost = cost;
        this._colour = colour;
        this._speed = 0;
        this._distanceTravelled = 0;
    }

    startEngine() {
        console.log('Engine Started');
        this.emit('startEngine');

        setInterval(()=>{
            this._distanceTravelled += 1;
            this.emit('travelling',this._distanceTravelled);
        }, 5000)
    };
}


class Telemetrics {
    constructor() {
        this._timesTracked = 0;
    }

    tracker(car) {
        this._timesTracked++;
        console.log("tracking " + this._timesTracked + " : " + car._make);
    }
}

let myVehicle = new Vehicle('Ford','Fiesta',19999,"Red");
let myTelemetrics = new Telemetrics();

myVehicle.on('startEngine',(evt)=>console.log('Engine started'));
myVehicle.on('travelling',(evt)=>myTelemetrics.tracker(myVehicle));

myVehicle.startEngine();

