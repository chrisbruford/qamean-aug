const mocha = require('mocha');
const expect = require('chai').expect;
const proxyquire = require('proxyquire');
const fs = require('fs');
const path = require('path');

// const courseController = require('../server/controllers/course-controller');
let courses = require('./courses.js');

let courseStub = {
    find: query => Promise.resolve(courses)
}

const courseController = proxyquire('../server/controllers/course-controller', { '../models/course': courseStub });

describe('course-controller', () => {
    describe('courseDownloadGenerator', () => {
        let promise;
        let dir = path.resolve(__dirname, '../server/public/courses/');

        beforeEach(() => {
            promise = courseController.courseDownloadGenerator();
        });

        afterEach(() => {
            promise.then(filename => {
                let url = `${dir}/${filename}`;
                fs.unlink(url, err => console.log);
            })
        });

        it('should return a URL', (done) => {
            expect(promise).to.be.a('promise');
            promise.then(url => {
                expect(url).to.be.a('string');
                done();
            });
        });

        it('should have created an output file', (done) => {
            promise.then(filename => {
                let url = `${dir}/${filename}`;
                fs.access(url, (err => {
                    expect(err).to.be.null;
                    done();
                }));
            });
        });

        it('should have written course data to the file', (done) => {
            promise.then(
                filename => {
                    let url = `${dir}/${filename}`;
                    fs.readFile(url, (err, data) => {
                        expect(err).to.be.null;
                        expect(JSON.parse(data)).to.not.throw;
                        done()
                    });
                });
        });
    });
});