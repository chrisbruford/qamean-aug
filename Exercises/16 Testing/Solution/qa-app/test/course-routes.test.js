const mocha = require('mocha');
const expect = require('chai').expect;
const sinon = require('sinon');
const request = require('supertest');
const server = require('../server/server');

describe('course-routes',()=>{
    describe('GET /api/courses/download',()=>{
        it('should return a file containing course JSON data',()=>{
            return request(server)
                .get('/api/courses/download')
                .expect('Content-Type', /json/)
                .expect(200)
                .then(res=>{
                    console.log(res);
                });
        });
    })
})