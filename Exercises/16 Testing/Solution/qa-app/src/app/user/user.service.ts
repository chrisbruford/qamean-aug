import { Injectable } from '@angular/core';
import { User } from './user.model';
import { HttpClient } from '@angular/common/http';
import { catchError } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { _throw  } from 'rxjs/observable/throw';
import { Credentials } from './credentials.model';
import { UserWithCreds } from './user-with-creds.model';
import { environment } from '../../environments/environment';

@Injectable()
export class UserService {

    user: Credentials = {
        username: "",
        password: ""
    }

    constructor(private http: HttpClient) { }

    authenticate(user: Credentials): Observable<User> {
        return this.http.post<User>(`${environment.API_URL}/auth/authenticate`, user).pipe(
            catchError(err => {
                console.log(err);
                return _throw(err)
            })
        );
    }

    logout(): Observable<boolean> {
        return this.http.get(`${environment.API_URL}/logout`).pipe(
            catchError(err => {
                console.log(err);
                return _throw(err);
            })
        );
    }

    register(newUser: UserWithCreds): Observable<User> {
        return this.http.post<User>(`${environment.API_URL}/auth/register`,newUser).pipe(
            catchError(err=>{
                console.log(err);
                return _throw(err);
            })
        )
    }

}
