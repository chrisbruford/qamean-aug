import { Component, OnInit } from '@angular/core';
import { User } from '../user.model';
import { UserWithCreds } from '../user-with-creds.model';
import { UserService } from '../user.service';
import { catchError } from 'rxjs/operators/catchError';
import { Router } from '@angular/router';

@Component({
    selector: 'app-register',
    templateUrl: './register.component.html',
    styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

    newUser: UserWithCreds = {
        firstname: "",
        surname: "",
        email: "",
        username: "",
        password: ""
    };

    constructor(private userService: UserService, private router: Router) { }

    ngOnInit() {    }

    doSubmit() {
        this.userService.register(this.newUser).subscribe(
            (user: User)=>{
                console.log(user);
                this.router.navigate(['/login']);
            },
            (err)=>{console.log(err.message)}
        );
    }

}
