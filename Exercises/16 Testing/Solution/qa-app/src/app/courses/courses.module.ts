import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CourseBrowseComponent } from './course-browse/course-browse.component';
import { CourseService } from 'app/courses/course.service';
import { CourseCreatorComponent } from './course-creator/course-creator.component';
import { CoursesRoutingModule } from 'app/courses/courses-routing.module';
import { CoursesComponent } from './courses.component';

@NgModule({
  imports: [
    CommonModule,
    CoursesRoutingModule
  ],
  declarations: [CourseBrowseComponent, CourseCreatorComponent, CoursesComponent],
  exports: [CourseBrowseComponent],
  providers: [CourseService]
})
export class CoursesModule { }
