const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const path = require('path');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const User = require('./models/user');
const authenticationRoutes = require('./routes/authentication');
const register = require('./routes/register');
const coursesRoutes = require('./routes/courses');
const db = require('./db.js');

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(bodyParser.json());
app.use(express.static(path.resolve(__dirname,'../dist')))

//initialise passport and its session cookie
app.use(passport.initialize());
app.use(passport.session());

//use passport-local
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

app.use('/api/courses',coursesRoutes);
app.use('/api/auth',authenticationRoutes);
app.use('/api/register',register);

app.get('/*',(req,res, next) => {
    res.sendFile(path.resolve(__dirname, `../dist/index.html`));
})

let server = app.listen(8080, () => {
    console.log('Server is alive on port 8080');
});

module.exports = server;