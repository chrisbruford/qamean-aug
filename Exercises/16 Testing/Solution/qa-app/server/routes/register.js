const router = require('express').Router();
const passport = require('passport');
const User = require('../models/user');

router.post('/',(req,res,next)=>{
    let newUser = {
        username: req.body.username,
        firstname: req.body.firstname,
        surname: req.body.surname,
        email: req.body.email
    }
    
    User.register(new User(newUser),req.body.password, (err, user)=>{
        if (err) {
            console.log(err);
            return res.json(500);
        }
        
        res.json(true);
    })
});

module.exports = router;