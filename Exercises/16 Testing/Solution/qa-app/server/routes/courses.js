const Course = require('../models/course');
const router = require('express').Router();
const courseController = require('../controllers/course-controller');
const path = require('path');
const fs = require('fs');

router.get('/download',(req,res, next)=>{
    courseController.courseDownloadGenerator()
        .then(filename=>{
            let filePath = `${path.resolve(__dirname,'../public/courses')}/${filename}`;
            res.sendFile(filePath,{
                headers: {
                    "Content-Disposition": "attachment"
                }
            });
        });
});

router.get('/', (req, res, next) => {
    console.log('api/courses requested');
    Course.find({},(err,courses)=>{
        if (err) { console.log('error'); return next(err) }
        console.log('sending response');
        res.json({data: courses});
    })
});

router.post('/', (req,res, next)=>{
    let course = (({title,code,delivery,days})=>({title,code,delivery,days}))(req.body);
    
    //instantiate a new Course from our mongoose model, passing in an object with 
    //the required properties
    let newCourse = new Course(course)
    newCourse.save((err,course)=>{
        if (err) { return next(err) }
    })
    res.sendStatus(201);
})

module.exports = router;