const router = require('express').Router();
const passport = require('passport');

router.post('/authenticate',passport.authenticate('local'),(req,res,next)=>{
    res.json(req.user);
});

router.get('/logout',(req,res,next)=>{
    req.logout();
    res.json(true);
})

module.exports = router;