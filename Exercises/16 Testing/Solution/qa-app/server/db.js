const mongoose = require('mongoose');

mongoose.connect('mongodb://localhost/qa')
.then(mongoose=>{
    console.log('Successfully Connected to DB');
})
.catch((err)=>{
    console.log('Failed to connect to DB')
});

module.exports = mongoose;