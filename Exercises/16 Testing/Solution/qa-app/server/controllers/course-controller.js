const Course = require('../models/course');

const fs = require('fs');
const path = require('path');
const uuid4 = require('uuid/v4');

class CourseController {
    courseDownloadGenerator() {
        return Course.find({})
            .then(courses => {
                return new Promise((resolve, reject) => {
                    let dir = path.resolve(__dirname, '../public/courses');
                    let uniqueFileName = uuid4();
                    let url = `${dir}/${uniqueFileName}.json`;
                    fs.writeFile(url, JSON.stringify(courses), () => {
                        resolve(`${uniqueFileName}.json`);
                    })
                });
            })
            .catch(err => {
                console.log(err);
                return Promise.reject(err);
            })
    }
}

module.exports = new CourseController();