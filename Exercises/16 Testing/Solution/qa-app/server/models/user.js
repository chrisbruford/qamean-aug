const mongoose = require('mongoose');
const passportLocalMongoose = require('passport-local-mongoose');

let schema = new mongoose.Schema({
    firstname: {
        type: String,
        required: true
    },
    surname: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    }
});

schema.plugin(passportLocalMongoose);

module.exports = mongoose.model('User',schema);
