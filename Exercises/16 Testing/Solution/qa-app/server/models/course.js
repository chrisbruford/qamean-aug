const mongoose = require('mongoose');

let schema = new mongoose.Schema({
    title: {
        type: String,
        required: true
    },
    code: {
        type: String,
        required: true
    },
    delivery: {
        type: String,
        required: true
    },
    days: {
        type: Number,
        required: true
    }
})

module.exports = mongoose.model('Course',schema);
