const fs = require("fs");
fs.readFile('data.json', 'utf-8', (err,data)=>{
    if (err) { return console.log(err) }
    
    try {
        data = JSON.parse(data);
    } catch (e) {
        return console.log(err);
    }

    let instructors = data.instructors;
    
    for (instructor of instructors) {
        console.log(instructor.name);
    }
})