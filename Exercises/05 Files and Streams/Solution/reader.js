const fs = require("fs");

let args = process.argv.slice(2);
let output = args.join(" ");

fs.writeFile('output.txt',output,{flag: 'wx'},(err)=>{
    if (err) {
        if (err.code === "EEXIST") {
            console.log('File already exists');
        } else {
            console.log('Some error occured');
        }
        return
    }
    console.log("Finished writing to file")
});

