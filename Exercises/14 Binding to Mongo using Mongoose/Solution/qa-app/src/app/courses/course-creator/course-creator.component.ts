import { Component, OnInit } from '@angular/core';
import { Course } from 'app/courses/course.model';

@Component({
    selector: 'app-course-creator',
    templateUrl: './course-creator.component.html',
    styleUrls: ['./course-creator.component.css']
})
export class CourseCreatorComponent implements OnInit {

    course: Course;

    constructor() { }

    ngOnInit() {
    }

}
