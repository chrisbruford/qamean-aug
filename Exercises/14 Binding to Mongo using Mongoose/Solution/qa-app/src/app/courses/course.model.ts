export class Course {
    title: string;
    code: string;
    delivery: string;
    days: number
}