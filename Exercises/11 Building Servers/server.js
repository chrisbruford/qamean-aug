const http = require('http');
http.createServer((req,res)=>{
    if (req.url === "/" && req.method === "GET") {
        res.statusCode = 200;
        res.end("Welcome Home");
    }
    else if (req.url === "/users" && req.method === "GET") {
        res.statusCode = 200;
        res.end("A list of users to follow.....");
    } 
    else if (req.url === "/courses" && req.method === "GET") {
        res.statusCode = 200;
        res.end("Courses are fun!");
    } 
    else if (req.url === "/coffee" && req.method === "GET") {
        res.statusCode = 418;
        res.statusMessage = "Short and Stout"
        res.end()
    }
    else {
        res.statusCode = 404;
        res.end();
    }
}).listen(8080,'127.0.0.1');

console.log('Server running on 127.0.0.1:8080');