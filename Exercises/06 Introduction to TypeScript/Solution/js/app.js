var __extends = (this && this.__extends) || (function () {
    var extendStatics = Object.setPrototypeOf ||
        ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
        function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Course = (function () {
    function Course(title, id, price) {
        this.title = title;
        this.id = id;
        this.price = price;
    }
    return Course;
}());
var Instructor = (function () {
    function Instructor(firstname, lastname, courses) {
        this.firstname = firstname;
        this.lastname = lastname;
        this.courses = courses;
    }
    return Instructor;
}());
var QAEvent = (function () {
    function QAEvent(_course, _instructor, _date, _capacity, _delegates) {
        this._course = _course;
        this._instructor = _instructor;
        this._date = _date;
        this._capacity = _capacity;
        this._delegates = _delegates;
    }
    Object.defineProperty(QAEvent.prototype, "course", {
        get: function () {
            return this._course;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(QAEvent.prototype, "instructor", {
        get: function () {
            return this._instructor;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(QAEvent.prototype, "date", {
        get: function () {
            return this._date;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(QAEvent.prototype, "capacity", {
        get: function () {
            return this._capacity;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(QAEvent.prototype, "delegates", {
        get: function () {
            return this._delegates;
        },
        enumerable: true,
        configurable: true
    });
    return QAEvent;
}());
var CBLEvent = (function (_super) {
    __extends(CBLEvent, _super);
    function CBLEvent(_course, _instructor, _date, _location, _capacity, _delegates) {
        var _this = _super.call(this, _course, _instructor, _date, _capacity, _delegates) || this;
        _this._course = _course;
        _this._instructor = _instructor;
        _this._date = _date;
        _this._location = _location;
        _this._capacity = _capacity;
        _this._delegates = _delegates;
        return _this;
    }
    Object.defineProperty(CBLEvent.prototype, "location", {
        get: function () {
            return this._location;
        },
        enumerable: true,
        configurable: true
    });
    return CBLEvent;
}(QAEvent));
var RAEvent = (function (_super) {
    __extends(RAEvent, _super);
    function RAEvent(_course, _instructor, _date, _vm, _capacity, _delegates) {
        var _this = _super.call(this, _course, _instructor, _date, _capacity, _delegates) || this;
        _this._course = _course;
        _this._instructor = _instructor;
        _this._date = _date;
        _this._vm = _vm;
        _this._capacity = _capacity;
        _this._delegates = _delegates;
        return _this;
    }
    Object.defineProperty(RAEvent.prototype, "vm", {
        get: function () {
            return this._vm;
        },
        enumerable: true,
        configurable: true
    });
    return RAEvent;
}(QAEvent));
var theCourse = new Course("Programming with TypeScript", "QATYPESCRIPT", 399999);
theCourse.description = "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi rhoncus condimentum ligula, a mattis tellus. Vestibulum rutrum, purus eget malesuada ultricies, leo ante laoreet ex, ac rutrum ante est posuere metus. Pellentesque semper eros id mauris placerat, nec placerat metus ultrices. Curabitur eget efficitur dolor. Integer condimentum rutrum mi sit amet euismod. Vestibulum efficitur finibus nunc. Pellentesque pellentesque eleifend diam at convallis. Sed volutpat commodo justo ut fermentum. In aliquet ipsum eget purus finibus, ut tempor mauris vestibulum.";
var theInstructor = new Instructor('Chris', 'Bruford', [theCourse]);
var theQAEvent = new CBLEvent(theCourse, theInstructor, "6th March 2017", "IH", 12, 5);
var anotherEvent = new CBLEvent(theCourse, theInstructor, "18th April 2017", "MS", 7, 2);
var remoteEvent = new RAEvent(theCourse, theInstructor, "21st May 2017", true, 4, 3);
createPricingTables([theQAEvent, anotherEvent, remoteEvent]);
function createPricingTables(events) {
    var container = document.querySelector('[data-qa-courses]');
    events.forEach(function (event) {
        var pricingTable = document.createElement("table");
        var courseRow = pricingTable.appendChild(document.createElement("tr"));
        var dateRow = pricingTable.appendChild(document.createElement("tr"));
        var instructorRow = pricingTable.appendChild(document.createElement("tr"));
        var locationRow = pricingTable.appendChild(document.createElement("tr"));
        var delegatesRow = pricingTable.appendChild(document.createElement("tr"));
        var capacityRow = pricingTable.appendChild(document.createElement("tr"));
        courseRow.innerHTML = "<td>Course</td><td>" + event.course.title + "</td>";
        dateRow.innerHTML = "<td>Date</td><td>" + event.date + "</td>";
        instructorRow.innerHTML = "<td>Instructor</td><td>" + event.instructor.firstname + " " + event.instructor.lastname + "</td>";
        delegatesRow.innerHTML = "<td>Delegates</td><td>" + event.delegates.toString() + "</td>";
        capacityRow.innerHTML = "<td>Capacity</td><td>" + event.capacity.toString() + "</td>";
        container.appendChild(pricingTable);
    });
}
