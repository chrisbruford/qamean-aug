const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const path = require('path');

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(bodyParser.json());
app.use(express.static(path.resolve(__dirname,'../dist')))

let courses = [
    {
        "title": "Building JavaScript Applications Using NodeJS and the MEAN Stack",
        "code": "QANODEDEV",
        "delivery": "Classroom",
        "days": 4
    },
    {
        "title": "Developing MVC single-page web applications using AngularJS",
        "code": "QAANGULARJS",
        "delivery": "Classroom",
        "days": 2
    },
    {
        "title": "Developing Web Applications Using HTML5",
        "code": "QAWEBUI",
        "delivery": "Classroom",
        "days": 5
    },
    {
        "title": "Leveraging the Power of JQuery",
        "code": "QAJQUERY",
        "delivery": "Classroom",
        "days": 3
    },
    {
        "title": "Programming with JavaScript",
        "code": "QAJAVSC",
        "delivery": "Classroom",
        "days": 5
    },
    {
        "title": "Next Generation JavaScript: ECMAScript2015",
        "code": "QAES2015",
        "delivery": "Classroom",
        "days": 2
    },
    {
        "title": "Building Web Applications Using Angular",
        "code": "QAANGULAR",
        "delivery": "Classroom",
        "days": 3
    }
]

app.get('/api/courses', (req, res) => {
    res.json({
        "data": courses
    })
});

app.post('/api/courses', (req,res)=>{
    //this is an immediatly invoked function expression that uses destructuring to 
    //take in only the properties that we're interested in from the object passed
    //and returns those properties within a new object
    let course = (({title,code,delivery,days})=>({title,code,delivery,days}))(req.body);

    //long-hand would be:
    // let {title, code, delivery, days} = req.body;
    // let course = {title, code, delivery, days};

    courses.push(course);
    res.sendStatus(201);
})

app.get('/*',(req,res) => {
    res.sendFile(path.resolve(__dirname, `../dist/index.html`));
})

app.listen(8080, () => {
    console.log('Server is alive on port 8080');
});