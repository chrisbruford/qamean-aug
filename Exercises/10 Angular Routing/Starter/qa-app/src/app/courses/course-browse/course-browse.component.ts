import { Component, OnInit } from '@angular/core';
import { CourseService } from 'app/courses/course.service';
import { Course } from 'app/courses/course.model';

@Component({
    selector: 'app-course-browse',
    templateUrl: './course-browse.component.html',
    styleUrls: ['./course-browse.component.css']
})
export class CourseBrowseComponent implements OnInit {

    courses: Course[]

    constructor(private courseService: CourseService) { }

    ngOnInit() {
        this.courseService.getCourses().subscribe(courses=>{
            this.courses = courses;
        })
    }

}
