import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CourseBrowseComponent } from 'app/courses/course-browse/course-browse.component';
import { CourseCreatorComponent } from 'app/courses/course-creator/course-creator.component';
import { CoursesComponent } from 'app/courses/courses.component';

let routes: Routes = [
    {path: 'courses', component: CoursesComponent,
    children: [
        {path: 'create', component: CourseCreatorComponent},
        {path: 'browse', component: CourseBrowseComponent},
    ]}
]

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  declarations: [],
  exports: [RouterModule]
})
export class CoursesRoutingModule { }
