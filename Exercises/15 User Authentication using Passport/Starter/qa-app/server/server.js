const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const path = require('path');
const Course = require('./models/course');

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
    next();
});

app.use(bodyParser.json());
app.use(express.static(path.resolve(__dirname,'../dist')))

app.get('/api/courses', (req, res, next) => {
    console.log('api/courses requested');
    Course.find({},(err,courses)=>{
        if (err) { console.log('error'); return next(err) }
        console.log('sending response');
        res.json({data: courses});
    })
});

app.post('/api/courses', (req,res, next)=>{
    let course = (({title,code,delivery,days})=>({title,code,delivery,days}))(req.body);
    
    //instantiate a new Course from our mongoose model, passing in an object with 
    //the required properties
    let newCourse = new Course(course)
    newCourse.save((err,course)=>{
        if (err) { return next(err) }
    })
    res.sendStatus(201);
})

app.get('/',(req,res, next) => {
    res.sendFile(path.resolve(__dirname, `../dist/index.html`));
})

app.listen(8080, () => {
    console.log('Server is alive on port 8080');
});