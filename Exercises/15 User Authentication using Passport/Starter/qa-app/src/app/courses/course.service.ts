import { Injectable } from '@angular/core';
import { Observable, Observer } from 'rxjs';
import { Course } from 'app/courses/course.model';
import { HttpClient } from '@angular/common/http';
import 'rxjs/add/operator/map'
import { environment } from 'environments/environment';

@Injectable()
export class CourseService {

    constructor(private http: HttpClient) { }

    getCourses(): Observable<Course[]> {
        return this.http.get<{ data: Course[] }>(`${environment.API_URL}/courses`).map(res => res.data);
    }
}
