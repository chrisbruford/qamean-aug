import { Component, OnInit } from '@angular/core';
import { User } from '../user.model';
import { Credentials } from '../credentials.model';
import { UserService } from '../user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
    credentials: Credentials = {
        username: "",
        password: ""
    }
    user: User;

  constructor(private userService: UserService) { }

  ngOnInit() {
  }

  doSubmit() {
    this.userService.authenticate(this.credentials).subscribe(user=>{
        this.user = user;
        console.log(user);
    },
    err=>{
        console.log(err.message);
    })
  }
}
