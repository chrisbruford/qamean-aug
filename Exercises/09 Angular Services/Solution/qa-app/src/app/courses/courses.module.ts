import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CourseBrowseComponent } from './course-browse/course-browse.component';
import { CourseService } from 'app/courses/course.service';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [CourseBrowseComponent],
  exports: [CourseBrowseComponent],
  providers: [CourseService]
})
export class CoursesModule { }
