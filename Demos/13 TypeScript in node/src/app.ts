import * as express from 'express';
import * as path from 'path';

const app = express();

app.use('/',(req,res,next)=>{
    const indexPath = path.resolve(__dirname,'../index.html');
    res.sendFile(indexPath);
});

app.listen('8080',()=>{
    console.log('Server is live');
});