const path = require('path');

module.exports = {
    entry: './src/app.ts',
    output: {
        path: path.resolve(__dirname, 'dist'),
        filename: 'app.js'
    },

    resolve: {
        extensions: ['.ts', '.js']
    },
    
    target: 'node',

    module: {
        rules: [
            { 
                test: /\.ts?$/, 
                loader: 'ts-loader'
            }
        ]
    }
}