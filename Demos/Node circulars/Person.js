const Dog = require('./Dog.js');

class Person {
    constructor() {
        this.pet = new Dog();
    }
}

module.exports = Person;