const http = require('http');

http.createServer(function (req, res) {
    if (req.url === '/') {
        res.writeHead(200, { 'Content-Type': 'text/plain' });
        res.end('Hello World\n');
    }
    else if (req.url === '/bananas') {
        res.writeHead(200, {'Content-Type': 'text/plain'});
        res.end('You\'re Bananas!');
    }
    else {
        res.writeHead(404,'Page not found');
        res.end();
    }


}).listen(8080, '127.0.0.1');

console.log('Server live on http://127.0.0.1:8080');