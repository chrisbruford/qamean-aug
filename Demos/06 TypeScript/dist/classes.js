var __extends = (this && this.__extends) || (function () {
    var extendStatics = function (d, b) {
        extendStatics = Object.setPrototypeOf ||
            ({ __proto__: [] } instanceof Array && function (d, b) { d.__proto__ = b; }) ||
            function (d, b) { for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p]; };
        return extendStatics(d, b);
    }
    return function (d, b) {
        extendStatics(d, b);
        function __() { this.constructor = d; }
        d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
    };
})();
var Vehicle = /** @class */ (function () {
    function Vehicle(gps, power) {
        this.gps = gps;
        this.wheels = 4;
        this._speed = 0;
        this.power = power;
        Vehicle.count++;
    }
    Vehicle.prototype.accelerate = function (speed) {
        this._speed = speed;
    };
    Vehicle.prototype.brake = function (speed) {
        this._speed = speed;
    };
    Object.defineProperty(Vehicle.prototype, "speed", {
        get: function () {
            return this._speed;
        },
        set: function (speed) {
            this._speed = speed;
        },
        enumerable: true,
        configurable: true
    });
    Vehicle.count = 0;
    return Vehicle;
}());
var Car = /** @class */ (function (_super) {
    __extends(Car, _super);
    function Car(gps, power) {
        return _super.call(this, gps, power) || this;
    }
    return Car;
}(Vehicle));
function parkCar(car) {
    car.brake(0);
}
// let myVehicle = new Vehicle(true);
var myCar = new Car(true, 500);
console.log(Vehicle.count);
// structural typing is allowed (but won't work with private props)
// parkCar({
//     gps: true,
//     power: 100,
//     speed: 0,
//     wheels: 4,
//     accelerate: (speed: number)=>{},
//     brake: (speed: number) => {},
// });
