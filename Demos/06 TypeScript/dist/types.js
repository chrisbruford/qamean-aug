//it is not recommended to add type definitions
//to primatives
var myString = 'A string';
var myNumber = 10;
var myBoolean = false;
var myObject = {};
var myUndefined;
var myNull = null;
//making errors
var myNewString = '33';
// let mySymbol2 = Symbol();
// {
//     let mySymbol = Symbol();
//     myObject[mySymbol] = "some pseudo-secret";
// }
// myObject[Symbol.iterator] = function() {
//     //iterator functionality
// }
//Arrays
// let myFavouriteThings: string[];
var myFavouriteThings;
myFavouriteThings = ["Scuba Diving", "Skiing", "Cycling"];
//Tuple
var details;
details = ["Chris", 23];
//Enum
var Color;
(function (Color) {
    Color[Color["Red"] = 0] = "Red";
    Color[Color["Green"] = 1] = "Green";
    Color[Color["Blue"] = 2] = "Blue";
    Color[Color["Purple"] = 3] = "Purple";
})(Color || (Color = {}));
;
var ColorNames;
(function (ColorNames) {
    ColorNames["Red"] = "red";
    ColorNames["Green"] = "green";
    ColorNames["Blue"] = "blue";
    ColorNames["Purple"] = "purple";
})(ColorNames || (ColorNames = {}));
var favouriteColours = [Color.Purple, Color.Blue];
var favouriteColours2 = [ColorNames.Purple, ColorNames.Blue];
//Functions
function printMessage(message) {
    console.log(message);
}
function printSomething(thing) {
    if (typeof thing === 'string') {
        console.log(thing.length);
    }
    else {
        thing.toFixed(1);
    }
}
printMessage(null);
function logError(err) {
    throw err;
}
// type assertion
var myThing = 50;
myThing.toFixed(2);
myThing.toFixed(2);
