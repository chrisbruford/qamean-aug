abstract class Vehicle {
    public wheels = 4;
    private _speed = 0;
    public readonly power: number;
    static count = 0;

    constructor(
        public gps: boolean,
        power: number
    ) {
        this.power = power;
        Vehicle.count++;
    }

    accelerate(speed: number) {
        this._speed = speed;
    }

    brake(speed: number) {
        this._speed = speed;
    }

    get speed() {
        return this._speed;
    }
    
    set speed(speed: number) {
        this._speed = speed;
    }
}

class Car extends Vehicle {
    constructor(gps: boolean, power: number) {
        super(gps, power);
    }
}

function parkCar(car: Car) {
    car.brake(0);
}

// let myVehicle = new Vehicle(true);
let myCar = new Car(true,500);

console.log(Vehicle.count);

// structural typing is allowed (but won't work with private props)
// parkCar({
//     gps: true,
//     power: 100,
//     speed: 0,
//     wheels: 4,
//     accelerate: (speed: number)=>{},
//     brake: (speed: number) => {},
// });

