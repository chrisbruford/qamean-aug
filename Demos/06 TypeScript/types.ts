//it is not recommended to add type definitions
//to primatives
let myString: string = 'A string';
let myNumber: number = 10;
let myBoolean: boolean = false;
let myObject: object = {};
let myUndefined: undefined;
let myNull: null = null;

//making errors
let myNewString: string = '33';

// let mySymbol2 = Symbol();

// {
//     let mySymbol = Symbol();
//     myObject[mySymbol] = "some pseudo-secret";
// }

// myObject[Symbol.iterator] = function() {
//     //iterator functionality
// }

//Arrays
// let myFavouriteThings: string[];
let myFavouriteThings: Array<string>;
myFavouriteThings = ["Scuba Diving", "Skiing", "Cycling"];

//Tuple
let details: [string, number];
details = ["Chris", 23];

//Enum
enum Color { Red, Green, Blue, Purple};
enum ColorNames { Red = "red", Green = "green", Blue = "blue", Purple = "purple" }

let favouriteColours: Color[] = [Color.Purple, Color.Blue];
let favouriteColours2: ColorNames[] = [ColorNames.Purple, ColorNames.Blue]

//Functions
function printMessage(message: string | null): void {
    console.log(message);
}

function printSomething(thing: string | number) {
    if (typeof thing === 'string') {
        console.log(thing.length);
    } else {
        thing.toFixed(1);
    }
}

printMessage(null);

function logError(err: Error): never {
    throw err;
}

// type assertion
let myThing: any = 50;
(<number>myThing).toFixed(2);
(myThing as number).toFixed(2);