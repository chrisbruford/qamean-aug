// console.log(`Filename: ${__filename}`);
// console.log(`Directory: ${__dirname}`);
// console.log(`CWD: ${process.cwd()}`);

// try {
//     process.chdir('/');
// } catch (err) {
//     console.error(err);
// }

const fs = require('fs');
// fs.readFile('randj.txt', 'utf8' ,(err,data)=>{
//     if (err) { return console.error(err.message); }
//     console.log(data);

//     fs.writeFile('randj-copy.txt',data,()=>{
//         console.log("EOF");
//     });
// });

//using streams

// const readStream = fs.createReadStream('randj.txt');
// const writeStream = fs.createWriteStream('randj-copy3.txt');

// readStream.on('error',console.error);

// readStream.on('data',data=>{
//     console.log(data.toString());
//     // writeStream.write(data);
// });

// readStream.on('end',()=>console.log('File end'));

// readStream.on('close',()=>console.log('Stream closed'));

// readStream.pipe(writeStream);

