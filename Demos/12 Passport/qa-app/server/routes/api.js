const express = require('express');
const router = express.Router();
const User = require('../models/user');
const passport = require('passport');

router.post('/login', passport.authenticate('local'),(req, res, next) => {
    // req.user only exists if authenticated
    // otherwise 401 Unauthorised
    res.json(req.user);
});

router.post('/register', (req, res, next) => {
    User.register(new User({
        username: req.body.username,
        email: req.body.email
    }),
        req.body.password,
        (err) => {
            if (err) {
                res.json(false);
                return console.error(err);
            }

            res.json(true);
        });
});

module.exports = router;