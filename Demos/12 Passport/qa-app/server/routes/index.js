var express = require('express');
var router = express.Router();
const path = require('path');

/* GET home page. */
//angular routes
router.get('/',(req,res,next)=>{
  const indexPath = path.resolve(__dirname,'../../dist/qa-app/index.html');
  res.sendFile(indexPath);
});

router.get('/supersecret',(req,res,next)=>{
  if (req.user) {
    //authenticated
  } else {
    //not authenticated
  }
})

module.exports = router;
