var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
const mongoose = require('mongoose');
const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const User = require('./models/user');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
const apiRouter = require('./routes/api');

//DB Connection
const db = mongoose.connection;

db.on('error',(err)=>{
  console.errror('Oh no! the DB fell over');
})

const dbUrl = 'mongodb://localhost:27017/qa';
mongoose.connect(dbUrl,{user:'chris',pass:'password'});

var app = express();

//passport config
app.use(passport.initialize());
app.use(passport.session());
passport.use(new LocalStrategy(User.authenticate()));
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());

const distPath = path.resolve(__dirname, '../dist/qa-app');
app.use(express.static(distPath));

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/api', apiRouter);
//you can block whole routes
// app.use('/api', passport.authenticate('local'), apiRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.sendStatus(err.status || 500);
});

module.exports = app;
