import { Injectable } from '@angular/core';
import { User } from './user.model';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(private http: HttpClient) { }

  register(user: User): Observable<boolean> {
    //send register request to API
    return this.http.post<boolean>('/api/register',user);
  }

  login(user: {username: string, password: string}): Observable<User> {
    //send login request to API
    return this.http.post<User>('/api/login',user);
  }
}
