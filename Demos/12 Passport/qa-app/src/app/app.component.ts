import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { UserService } from './user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  user = '';
  pass = '';

  constructor(
    private userService: UserService,
    private router: Router
  ) { }

  doLogin() {
    this.userService.login({ username: this.user, password: this.pass })
      .subscribe(user => {
        if (user) {
          this.router.navigate(['/dashboard']);
        } else {
          //???
        }
      })
  }
}
