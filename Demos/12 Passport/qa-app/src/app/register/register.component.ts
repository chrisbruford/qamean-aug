import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { User } from '../user.model';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  newUser = new User();

  constructor(private userService: UserService) { }

  ngOnInit() {
  }

  doRegister() {
    this.userService.register(this.newUser)
      .subscribe((res) => {
        if (res) {
          console.log('registered');
        } else {
          console.log('failed....');
        }
      },
        (err) => {
          console.error('HTTP Error', err);
        }
      )
  }

}
