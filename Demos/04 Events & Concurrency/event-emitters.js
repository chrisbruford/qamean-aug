const EventEmitter = require('events').EventEmitter;
// const emitter = new EventEmitter();

// let x = 0;

// emitter.on('count',x=>console.log(x));

// emitter.emit('count',x++);
// emitter.emit('count',x++);
// emitter.emit('count',x++);
// emitter.emit('count',x++);

//class-based inheritance

// class Car extends EventEmitter {
//     constructor() {
//         super();
//         this.speed = 0;
//     }

//     accelerate(speed) {
//         this.speed = speed;
//         this.emit('accelerate',this.speed);
//     }
// }

// let myCar = new Car();
// myCar.on('accelerate',speed=>console.log(`Vrooom! ${speed}`));

// myCar.accelerate(30);
// myCar.accelerate(60);
// myCar.accelerate(90);

//composition

class Car {
    constructor() {
        this.speed = 0;
    }

    accelerate(speed) {
        this.speed = speed;
        this.emit('accelerate',speed);
    }
}

let myCar = new Car();
myCar.on('accelerate',speed=>console.log(`Vrooom! ${speed}`));
myCar.accelerate(30);
myCar.accelerate(60);
myCar.accelerate(90);