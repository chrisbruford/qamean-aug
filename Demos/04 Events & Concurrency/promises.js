//framework things
// function doThings() {
//     return new Promise((resolve, reject) => {
//         setTimeout(() => {
//             if (Math.random() < 0.5) {
//                 resolve("hello world");
//             } else {
//                 reject(new Error("Woops!"));
//             }
//         }, 1000);
//     });
// }

// let myPromise = doThings();

// myPromise
//     .then((data) => {
//         return doThings();
//     })
//     .then((data)=>{
//         console.log(data);
//     })
//     .catch(console.error);

// Promise.all([doThings(), doThings(), doThings()])
//     .then(array => {
//         console.log(array[0]);
//         console.log(array[1]);
//         console.log(array[2]);
//     })
//     .catch(console.error);

// function get(url) {
//     let xhr = new XMLHttpRequest();
//     xhr.open('GET',url);
    
//     let promise = new Promise((resolve,reject)=>{
//         xhr.onload = () => resolve(xhr.responseText);
//     });

//     xhr.send();
//     return promise;
// }

// get('https://jsonplaceholder.typicode.com/users')
//     .then(res=>console.log(JSON.parse(res)));