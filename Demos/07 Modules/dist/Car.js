var Car = /** @class */ (function () {
    function Car(speed) {
        this.speed = speed;
    }
    Car.prototype.accelerate = function () { };
    return Car;
}());
export { Car };
