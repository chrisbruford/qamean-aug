System.register("Car", [], function (exports_1, context_1) {
    "use strict";
    var Car;
    var __moduleName = context_1 && context_1.id;
    return {
        setters: [],
        execute: function () {
            Car = /** @class */ (function () {
                function Car(speed) {
                    this.speed = speed;
                }
                Car.prototype.accelerate = function () { };
                return Car;
            }());
            exports_1("Car", Car);
        }
    };
});
System.register("app", ["Car"], function (exports_2, context_2) {
    "use strict";
    var Car_1, myCar;
    var __moduleName = context_2 && context_2.id;
    return {
        setters: [
            function (Car_1_1) {
                Car_1 = Car_1_1;
            }
        ],
        execute: function () {
            myCar = new Car_1.Car(0);
            myCar.accelerate();
            console.log(myCar);
        }
    };
});
