const express = require('express');
const app = express();
const vehicles = require('./routes/vehicles');

app.get('/',(req,res,next)=>{
    console.log('I received a request');
    next();
});

app.get('/',(req,res,next)=>{
    res.send('Hello world');
    next();
});

app.use('/',(req,res,next)=>{
    console.log(`I've been called`);
    next();
});

app.use('/logthethings',logRequest,logBody);

app.use('/vehicles',vehicles);
app.use('/public', require('./routes/public'));
app.use('/assets', express.static('assets'));

function logRequest(req,res,next) {
    console.log(req);
    next();
}

function logBody(req,res,next) {
    console.log(req.body);
    next();
}

const server = app.listen(8080, ()=>{
    console.log(`Server listening on ${server.address().address}:${server.address().port}`);
});