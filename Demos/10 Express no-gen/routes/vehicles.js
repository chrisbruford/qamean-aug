const express = require('express');
const router = express.Router();

router.get('/car', (req, res, next) => {
    res.header({ 'Content-Type': 'text/html' });
    res.send('<h1>Cars</h2>');
});

router.get('/bus', (req, res, next) => {
    res.header({ 'Content-Type': 'text/html' });
    res.send('<h1>Wheels on the bus go round and round</h2>');
});

router.get('/:vehicle', (req, res, next) => {
    res.header({ 'Content-Type': 'text/html' });
    res.send(`<h1>Hmm yes I like ${req.params.vehicle}s too`);
})

module.exports = router;