const express = require('express');
const router = express.Router();
const path = require('path');

router.get('/romeoandjuliet',(req,res,next)=>{
    let assets = path.resolve(__dirname, '../assets');
    res.sendFile(`${assets}/randj.txt`);
});

module.exports = router;