import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { User } from 'src/app/user/user.model';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class SuperUserService {
  constructor(private http: HttpClient) { }

  getUsers(): Observable<User[]> {
    return this.http.get<User[]>('https://jsonplaceholder.typicode.com/users')
      .pipe(
        map(users=>{
          return users.map(user=>{
            user.email = "***";
            return user;
          })
        })
      )
  }
}