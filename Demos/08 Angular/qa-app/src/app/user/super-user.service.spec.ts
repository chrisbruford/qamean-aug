import { TestBed, inject } from '@angular/core/testing';

import { SuperUserService } from './super-user.service';

describe('SuperUserService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SuperUserService]
    });
  });

  it('should be created', inject([SuperUserService], (service: SuperUserService) => {
    expect(service).toBeTruthy();
  }));
});
