import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { UserService } from '../user.service';
import { User } from '../user.model';
import { switchMap } from 'rxjs/operators';

@Component({
  selector: 'app-view-user',
  templateUrl: './view-user.component.html',
  styleUrls: ['./view-user.component.css']
})
export class ViewUserComponent implements OnInit {
  user: User;

  constructor(
    private route: ActivatedRoute,
    private userService: UserService
  ) { }

  ngOnInit() {
    // this.route.params
    //   .subscribe((params: { userid: string }) => {
    //     this.userService.getUser(params.userid)
    //       .subscribe(user => {
    //         this.user = user;
    //       })
    //   });
    console.log(this.userService);

    this.route.params
      .pipe(
        switchMap((params: { userid: string }) => {
          return this.userService.getUser(params.userid);
        })
      )
      .subscribe(user => this.user = user);


  }



}
