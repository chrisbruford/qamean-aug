import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DisplayUsersComponent } from './display-users/display-users.component';
import { UserService } from 'src/app/user/user.service';
import { UserRoutingModule } from './user-routing.module';
import { AddUserComponent } from './add-user/add-user.component';
import { ViewUserComponent } from './view-user/view-user.component';

@NgModule({
  imports: [
    CommonModule,
    UserRoutingModule
  ],
  exports: [DisplayUsersComponent],
  declarations: [DisplayUsersComponent, AddUserComponent, ViewUserComponent],
})
export class UserModule { }
