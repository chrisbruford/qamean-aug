import { Component } from '@angular/core';
import { Observable, Observer,Subscription } from 'rxjs';
import { takeWhile } from 'rxjs/operators';
import { OnDestroy } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy {
  title = 'My Fantastic Homepage';
  displayGreeting = false;
  date: Date;
  dateSubscription: Subscription
  alive = true;

  user = {
    firstname: "Scott",
    surname: "Fulford"
  }

  constructor() {
    let date$: Observable<Date> = Observable.create((observer: Observer<Date>)=>{
      let n = 0;

      let timer = setInterval(()=>{
        observer.next(new Date());
        // if (n++ > 10) {
          // observer.complete();
          // clearTimeout(timer);
        // }
      },1000)
    });

    //if yuo want to keep a ref to the subscription
    // this.dateSubscription = date$.subscribe(
    //   date => this.date = date,
    //   err => console.error(err),
    //   () => console.log('All done')
    // );

    //or you can just....
    date$
    .pipe(
      takeWhile(()=>this.alive)
    )
    .subscribe(
      date => this.date = date,
      err => console.error(err),
      () => console.log('All done')
    );
    
  }

  ngOnDestroy() {
    this.dateSubscription.unsubscribe();
    this.alive = false;
  }
}