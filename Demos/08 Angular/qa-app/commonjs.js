//ES2015 (Also used in TypeScript)
import { Component } from '@angular/core';
console.log(Component);

//analagous to the above
//COMMONJS
let angular = require('@angular/core');
let Component = angular.Component;
console.log(Component);