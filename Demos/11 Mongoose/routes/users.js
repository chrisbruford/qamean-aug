var express = require('express');
var router = express.Router();
const User = require('../models/user');

/* GET users listing. */
router.get('/', function (req, res, next) {
  User.find({}).then(users => {
    res.json(users);
  })
});

router.post('/',function(req,res,next) {
  let user = User.create({
    id: req.body.id,
    name: req.body.name,
    username: req.body.username,
    email: req.body.email
  })
  .then(() => res.sendStatus(200))
  .catch(err=> {
    console.log(err);
    res.sendStatus(500);
  });

  
})

module.exports = router;
